.PHONY: install yaml_to_json generate_client generate_server generate_docs view_docs view_docs_redoc_internal view_docs_redoc_external view_docs_basic view_docs_swagger_ui run_server run_client run_upload_test

UPLOAD_TEST_FILE := go-server-generated/uploads/go-logo-blue.svg

install:
	npm install -g swagger-cli
	wget https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.21/swagger-codegen-cli-3.0.21.jar -O swagger-codegen-cli.jar
	
yaml_to_json:
	swagger-cli bundle -o specs/file-upload-api.json specs/file-upload-api.yml
	
generate_client:
	java -jar swagger-codegen-cli.jar generate \
		-i specs/file-upload-api.yml \
		-l go \
		-o go-client-generated
	
generate_server:
	java -jar swagger-codegen-cli.jar generate \
		-i specs/file-upload-api.yml \
		-l go-server \
		-o go-server-generated2
		
generate_docs:
	java -jar swagger-codegen-cli.jar generate \
		-i specs/file-upload-api.yml \
		-l html2 \
		-o docs
	mv docs/index.html docs/file_upload_api_docs.html	
		
view_docs_redoc_internal:
	xdg-open 'http://localhost:8080/docs'

view_docs_redoc_external:
	docker rm -f swagger-redoc || true
	docker run --name swagger-redoc -d -p 8082:80 -e SPEC_URL=swagger/file-upload-api.yml -v $(PWD)/specs/:/usr/share/nginx/html/swagger/ redocly/redoc
	xdg-open 'http://localhost:8082'
	
view_docs_basic:
	xdg-open docs/file_upload_api_docs.html
	
view_docs_swagger_ui:
	docker rm -f swagger-ui || true
	docker run --name swagger-ui -d -p 8081:8080 -e SWAGGER_JSON=/specs/file-upload-api.json -v $(PWD)/specs:/specs swaggerapi/swagger-ui
	xdg-open 'http://localhost:8081'
	
view_docs: view_docs_redoc_internal

run_server:	
	cd go-server-generated && go run main.go
	
run_client:
	echo "Not implemented yet"
	
run_upload_test:
	curl -vi -H 'Connection: Close' -F author=Curl-Client -F file=@go-logo-blue.svg http://localhost:8080/v1/file
ifneq ("$(wildcard $(UPLOAD_TEST_FILE))","")
	@echo "Upload succeeded!"
else
	@echo "Upload failed"
	exit 1
endif
