# File upload REST API microservice

## Task

Write a micro-service with HTTP Swagger API that uploads files.

## Dependencies

- Go
- [Gorilla Mux](https://github.com/gorilla/mux)

## OpenAPIv3 spec

See [file-upload-api.yml](file-upload-api.yml) or [file-upload-api.json](file-upload-api.json)

## Test

### CLI

`curl -v -F author=Curl-Client -F file=@go-logo-blue.svg http://localhost:8080/v1/file`

### Web

`xdg-open test_file_upload.html`

### Swagger Go generated client

```
cd go-client-generated
```

### Swagger Go generated server

```
cd go-server-generated
go run main.go
```

## [Bricks](https://github.com/pace/bricks)

Help:

```
$ go get github.com/pace/bricks/cmd/pb

$ pb generate rest -h
Usage:
pb generate rest [flags]

Flags:
-h, --help help for rest
--path string path for generated file
--pkg string name for the generated go package
--source string OpenAPIv3 source to use for generation
```

How to:

```
npm install -g swagger-cli
swagger-cli bundle -o file-upload-api.json file-upload-api.yml
pb generate rest --source file-upload-api.json --path api/file_upload_api.go --pkg api
gofmt -w api/file_upload_api.go
```

## Other code generators
- https://github.com/pace/bricks
- https://github.com/wI2L/fizz
- https://github.com/deepmap/oapi-codegen

## Resources

### Tools

- [Swagger Editor](https://swagger.io/tools/swagger-editor/)
- [Swagger UI](https://swagger.io/tools/swagger-ui/)
- [Swagger Codegen](https://swagger.io/tools/swagger-codegen/)
  - [Online Swagger Generator](https://generator.swagger.io/)
- [Swagger Hub](https://swagger.io/tools/swaggerhub/)
- [Swagger Inspector](https://swagger.io/tools/swagger-inspector/)
- Visual Studio Code extensions:
  - [OpenAPI / Swagger Editor](https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi)
  - [Swagger Viewer](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer)
- [Redoc](https://github.com/Redocly/redoc)

### File upload

- https://swagger.io/docs/specification/describing-request-body/file-upload/
- https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md#considerations-for-file-uploads
- https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md#special-considerations-for-multipart-content

### Authentication and Authorization

- https://swagger.io/docs/specification/authentication/
- https://swagger.io/docs/specification/authentication/bearer-authentication/
- https://swagger.io/docs/specification/authentication/oauth2/
- https://jwt.io/
