# TODO

[ ] Handle error 422 Unprocessable Entity "Uploaded file was empty"
[ ] Handle error 422 Unprocessable Entity "The Length of file name exceeds permissible limit"
[ ] Handle error 400 Bad Request "File name contains special characters"
[ ] Handle error 500 InternalServerError "The server failed to read the uploaded file from disk"
[ ] Add PUT request to add file if it doesn't exit or replace file if exists and is newer, if its older a 409 Conflict should be returned
[ ] Add Docker support  
[ ] Add authentication  
[ ] Implement Go client  
https://gist.github.com/mattetti/5914158/f4d1393d83ebedc682a3c8e7bdc6b49670083b84
[ ] Add automated tests for file uploading  
[ ] Add API endpoint to upload multiple files at once
