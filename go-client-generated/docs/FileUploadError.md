# FileUploadError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **int32** | The internal code of the error | [optional] [default to null]
**Type_** | **string** | The type of error | [optional] [default to null]
**Message** | **string** | The detailed error essage | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

