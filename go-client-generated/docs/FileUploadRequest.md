# FileUploadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**File** | [****os.File**](*os.File.md) | Content of the file | [optional] [default to null]
**Author** | **string** | Author of the file | [optional] [default to null]
**Description** | **string** | Description of what the file is about | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

