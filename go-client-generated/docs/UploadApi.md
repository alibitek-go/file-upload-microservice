# {{classname}}

All URIs are relative to *https://localhost:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UploadFile**](UploadApi.md#UploadFile) | **Post** /file-upload | Upload file

# **UploadFile**
> FileUploadResponse UploadFile(ctx, file, author, description)
Upload file

Upload a single file to the server. All file types are supported.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **file** | ***os.File*****os.File**|  | 
  **author** | **string**|  | 
  **description** | **string**|  | 

### Return type

[**FileUploadResponse**](FileUploadResponse.md)

### Authorization

[file_upload_auth](../README.md#file_upload_auth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

