/*
 * File Upload API
 *
 * REST API for uploading files.
 *
 * API version: 1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"

	"github.com/google/uuid"
)

const uploadsPath = "uploads"

func init() {
	ensure_valid_uploads_path()
}

// directoryExists checks if the provided filepath exists and is a directory
func directoryExists(filePath string) bool {
	if filePath == "" {
		return false
	}
	info, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

func fileExists(filePath string) bool {
	if filePath == "" {
		return false
	}
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

// ensure_valid_uploads_path makes sure uploads path directory exists and creates it if it doesn't
func ensure_valid_uploads_path() {
	if !directoryExists(uploadsPath) {
		if err := os.MkdirAll(uploadsPath, 0775); err != nil {
			log.Fatalf("cannot create uploads directory err: %s\n", err)
		}
	}
}

// saveUploadedFile uploads the form file to specific dst.
func saveUploadedFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	return err
}

func jsonify(data interface{}) ([]byte, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	return jsonData, nil
}

// getFileID returns an ID for the given filepath
func getFileID(filePath string) (string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

func writeError(w http.ResponseWriter, code int, err FileUploadError) {
	w.WriteHeader(code)
	jsonData, _ := jsonify(err)
	w.Write(jsonData)
}

// UploadFile uploads a file
func UploadFile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	file, header, err := r.FormFile("file")
	if err != nil {
		writeError(w, http.StatusBadRequest, FileUploadError{
			ID:      uuid.New().String(),
			Status:  http.StatusText(http.StatusBadRequest),
			Code:    "Client Bad",
			Message: fmt.Sprintf("Missing 'file' field in request body for for uploaded file: %s", header.Filename)})
		return
	}
	file.Close()

	author := r.FormValue("author")
	if author == "" {
		writeError(w, http.StatusBadRequest, FileUploadError{
			ID:      uuid.New().String(),
			Status:  http.StatusText(http.StatusBadRequest),
			Code:    "Client Bad",
			Message: fmt.Sprintf("Missing 'author' field in request body for uploaded file: %s", header.Filename)})
		return
	}
	description := r.FormValue("description")

	fileUploadsPath := fmt.Sprintf("%s/%s", uploadsPath, header.Filename)

	if fileExists(fileUploadsPath) {
		writeError(w, http.StatusConflict, FileUploadError{
			ID:      uuid.New().String(),
			Status:  http.StatusText(http.StatusConflict),
			Code:    "Conflict",
			Message: fmt.Sprintf("Cannot save uploaded file: %s, file already exists! Suggestion: Use PUT method to replace it", header.Filename)})
		return
	}

	log.Printf("[Header] Filename: %s, Mimetype: %+v, Size: %d\n", header.Filename, header.Header.Get("Content-Type"), header.Size)
	log.Printf("[Form] Author: %s, Description: %s\n", author, description)

	if err := saveUploadedFile(header, fileUploadsPath); err != nil {
		writeError(w, http.StatusInternalServerError, FileUploadError{
			ID:      uuid.New().String(),
			Status:  http.StatusText(http.StatusInternalServerError),
			Code:    "Server Bad",
			Message: fmt.Sprintf("Cannot save uploaded file: %s", header.Filename)})
		return
	}

	w.WriteHeader(http.StatusCreated)

	fileID, err := getFileID(fileUploadsPath)
	if err != nil {
		writeError(w, http.StatusInternalServerError, FileUploadError{
			ID:      uuid.New().String(),
			Status:  http.StatusText(http.StatusInternalServerError),
			Code:    "Server Bad",
			Message: fmt.Sprintf("Cannot get file id for uploaded file: %s", header.Filename)})
		return
	}
	jsonData, _ := jsonify(FileUploadResponse{ID: fileID})
	w.Write(jsonData)
}
