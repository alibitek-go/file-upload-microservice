openapi: 3.0.2
info:
  title: File Upload API
  description: REST API for uploading files.
  license:
    name: MIT
    url: https://choosealicense.com/licenses/mit/
  version: "1.0"
servers:
- url: https://localhost:8080/v1
tags:
- name: upload
  description: File upload
paths:
  /file-upload:
    post:
      tags:
      - upload
      summary: Upload file
      description: Upload a single file to the server. All file types are supported.
      operationId: uploadFile
      requestBody:
        description: File and its metadata to be uploaded
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/FileUploadRequest'
        required: true
      responses:
        "201":
          description: File uploaded
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FileUploadResponse'
        "400":
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FileUploadError'
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FileUploadError'
        "500":
          description: Internal Server Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FileUploadError'
      security:
      - file_upload_auth:
        - upload:files
components:
  schemas:
    FileUploadRequest:
      type: object
      properties:
        file:
          type: string
          description: Content of the file
          format: binary
        author:
          type: string
          description: Author of the file
        description:
          type: string
          description: Description of what the file is about
      description: FileUploadRequest is the file upload request model
    FileUploadResponse:
      type: object
      properties:
        ID:
          type: string
          description: The unique id of the uploaded file
      description: FileUploadResponse is the file upload success response model
      example:
        ID: ID
    FileUploadError:
      type: object
      properties:
        code:
          type: integer
          description: The internal code of the error
          format: int32
        type:
          type: string
          description: The type of error
        message:
          type: string
          description: The detailed error essage
      description: FileUploadError is the file upload failure response model
  securitySchemes:
    file_upload_auth:
      type: oauth2
      flows:
        implicit:
          authorizationUrl: https://localhost:8080/api/oauth/dialog
          scopes:
            upload:files: upload files scope
