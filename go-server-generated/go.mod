module gitlab.com/alibitek-go/file-upload-microservice

go 1.14

require (
	github.com/go-openapi/runtime v0.19.21
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
)
